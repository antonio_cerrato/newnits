#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  1 11:58:19 2021

@author: Antonio Cerrato Casado
@email: antonioccdb@gmail.com
"""

import numpy as np


class UnitSystem(object):
    """ The referece system is the SI ,
    so length should be introduced in [m],
    mass in [kg],
    time in [s], etc."""
    
    __slots__ = ['__unit_length', 
                 '__unit_area', 
                 '__unit_volume',
                 '__unit_mass',
                 '__unit_density',
                 '__unit_energy',
                 '__unit_time',
                 '__unit_velocity',
                 '__unit_acceleration',
                 '__unit_force',
                 '__unit_stress',
                 '__unit_dynamic_viscosity',
                 '__unit_kinematic_viscosity',
                 '__density',
                 '__T',
                 '__molar_mass',
                 '__n_dof']
    
    units_SI = ['m', 
                'm^2', 
                'm^3',
                'kg',
                'kg/m^3',
                'J',
                's',
                'm/s',
                'm/s^2',
                'N',
                'N/m^2',
                'kg/(m·s) o Pa·s',
                'm^2/s',
                'kg/m^3',
                'Kelvin',
                'kg/mol',
                'degrees of freedom (molecule)']
    
    # Boltzman constant:
    _kB = 1.38064852*1e-23 # m2 kg s-2 K-1
    # Avogadro Number:
    _No = 6.02214076e+23 # mol-1
    
    @property
    def T(self):
        return self.__T
    @T.setter
    def T(self, value):
        self.__T = value
    
    @property
    def unit_length(self):
        return self.__unit_length
    @unit_length.setter
    def unit_length(self, value):
        self.__unit_length = value
        self.__unit_area = value*value
        self.__unit_volume = self.unit_area * value
        
    @property
    def unit_area(self):
        return self.__unit_area
    
    @unit_area.setter
    def unit_area(self, value):
        self.unit_length = np.sqrt(value)
    
    @property
    def unit_volume(self):
        return self.__unit_volume
    
    @unit_volume.setter
    def unit_volume(self, value):
        self.unit_length = value**(1/3)

    @property
    def density(self):
        return self.__density
    @density.setter
    def density(self, value):
        self.__density = value
        if hasattr(self, 'unit_length'):
            self.__unit_mass = self.__density*self.unit_volume 

    @property
    def unit_density(self):
        return self.__unit_density
    @unit_density.setter
    def unit_density(self, value):
        self.__unit_density = value
        
    @property
    def unit_mass(self):
        if not hasattr(self, '__unit_mass'):
            self.__unit_mass = self.density*self.unit_volume
        return self.__unit_mass
    @unit_mass.setter
    def unit_mass(self, value):
        self.__unit_mass = value
        self.__unit_density = self.__unit_mass/self.unit_volume
            
    @property
    def unit_energy(self):
        return self.__unit_energy
    
    @unit_energy.setter
    def unit_energy(self, value):
        if value == 'thermal':
            mol_kg = 1 / self.molar_mass
            moleculas_in_unit_vol = self.unit_volume * self.density* mol_kg * self._No
            value = 0.5*self.n_dof * self.T * self._kB * moleculas_in_unit_vol
            
        self.unit_time = np.sqrt(self.unit_mass*self.unit_length**2 / value)

    @property
    def molar_mass(self):
        return self.__molar_mass
    @molar_mass.setter
    def molar_mass(self, value):
        self.__molar_mass = value  
        
    @property
    def n_dof(self):
        return self.__n_dof
    @n_dof.setter
    def n_dof(self, value):
        self.__n_dof = value        
        
    @property
    def unit_force(self):
        return self.__unit_force
    @unit_force.setter
    def unit_force(self, value):
        self.unit_energy = value * self.unit_length
        
    @property
    def unit_dynamic_viscosity(self):
        return self.__unit_dynamic_viscosity
    @unit_dynamic_viscosity.setter
    def unit_dynamic_viscosity(self, value):
        self.__unit_dynamic_viscosity = value
        self.__unit_kinematic_viscosity = value/self.density

    @property
    def unit_kinematic_viscosity(self):
        return self.__unit_kinematic_viscosity
    @unit_kinematic_viscosity.setter
    def unit_kinematic_viscosity(self, value):
        self.__unit_kinematic_viscosity = value
        self.__unit_dynamic_viscosity = value*self.density
        
    @property
    def unit_time(self):
        return self.__unit_time
    @unit_time.setter
    def unit_time(self, value):
        # unit_length and unit_mass should be already defined.
        self.__unit_time = value
        self.__unit_velocity = self.unit_length/self.__unit_time
        self.__unit_acceleration = self.unit_length/self.__unit_time**2
        self.__unit_force = self.unit_mass*self.unit_acceleration
        self.__unit_stress = self.unit_force/self.unit_area
        self.__unit_energy = self.__unit_force*self.unit_length
        self.__unit_dynamic_viscosity = self.unit_mass/(self.unit_length*self.__unit_time)
        self.__unit_kinematic_viscosity = self.unit_length**2 /self.__unit_time
        
    @property
    def unit_velocity(self):
        return self.__unit_velocity
    @unit_velocity.setter
    def unit_velocidy(self, value):
        self.__unit_velocity = value
        
    @property
    def unit_acceleration(self):
        return self.__unit_acceleration
    @unit_acceleration.setter
    def unit_acceleration(self, value):
        self.__unit_acceleration = value
        
    @property
    def unit_stress(self):
        return self.__unit_stress
    @unit_stress.setter
    def unit_stress(self):
        self.__unit_stress = self.unit_force / self.unit_area
    
    
    def __init__(self, **kwargs):
        
        # Water releated properties:
        #################################
        self.T = 300
        self.molar_mass = 18.01528*1e-3
        self.n_dof = 6
        self.density = 1000
        #################################
        
        if 'unit_length' in kwargs:
            self.unit_length = kwargs['unit_length']
        

        if 'density' in kwargs:
            self.density = kwargs['density']

        if 'unit_mass' in kwargs:
            self.unit_mass = kwargs['unit_mass']
        else:
            # Default unit_mass:
            self.unit_mass = self.density * self.unit_volume
            
        if 'unit_energy' in kwargs:
            self.unit_energy = kwargs['unit_energy']
            
        for key, value in kwargs.items():
            setattr(self, key, value)
            
    def print_units(self):
        k=0
        for __attr in self.__slots__:
            attr = __attr[2:]    
            print(attr, ' = ', getattr(self, attr), ' ',self.units_SI[k] )
            k+=1
            
        pass
    
    
    
if __name__ == "__main__":
    
    """
    
    system1 = UnitSystem()
    system1.unit_length = 1e-6
    system1.density = 1000 
    #system1.hola = 5
    print(dir(system1))
    system1.unit_energy
    #print(system1.unit_energy)
    
    """
    
    print("========================")
    print("CGS system:")
    system_cgs = UnitSystem(unit_length = 1e-2, unit_mass=1e-1, unit_time = 1)
    system_cgs.print_units()
    print(system_cgs.unit_mass / system_cgs.unit_volume)
    print(system_cgs.unit_density)

    system_si = UnitSystem(unit_length = 1, unit_mass=1, unit_time = 1)
    system_si.print_units()
    
    system_cgs = UnitSystem(unit_length = 0.01, unit_mass=1e-3, unit_time = 1)
    system_cgs.print_units()
     
    unit_length = 5e-8
    unit_mass = 1000*unit_length**3
    coatsystem = UnitSystem(unit_length = unit_length, unit_mass=unit_mass,unit_time = 1e-3)
    coatsystem.density = 1000
    
    
    #coatsystem = UnitSystem(unit_length = unit_length, unit_mass=unit_mass,unit_energy = 'thermal')
    coatsystem.print_units()
    
    
    print("First Brush: strong forces?")
    
    r = 2.5e-8
    vol_s = 4/3*np.pi*r**3
    m_s = 1000*vol_s
    F_buoyancie = 9.8*m_s
    
    print("buoyancy force empty sphere = ", F_buoyancie, "N")
    


    print("========================================")
    print("========================================")
    print("========================================")
    print("Washing machine 2020")
    print("========================================")
    print("========================================")
    print("========================================")
    unit_length = 1e-5
    unit_mass = 1000*unit_length**3
    wmsystem = UnitSystem(unit_length = unit_length, unit_mass=unit_mass,unit_time = 1e-3)    
    
    print("REDUCED UNITS:")
    print("lengt fluid box = 1e-4m = ", 1e-4/wmsystem.unit_length)
    print("dynamic viscosity = ", 1e-3/wmsystem.unit_dynamic_viscosity)
    print("density 1000kg/m3 = ", 1e3/wmsystem.unit_density)
    print("Young modulus solids 1Pa = ", 1/wmsystem.unit_stress)
    print("Time step = 1e-2 = ", 1e-2/wmsystem.unit_time)
    print("Shear rate = 5s-1 =", 5*wmsystem.unit_time)
    
    # density water = 1000kg/m3
    
    
    
    print("========================================")
    print("========================================")
    print("========================================")
    print("Glycocalyx 2020")
    print("========================================")
    print("========================================")
    print("========================================")
    unit_length = 6.4e-9
    unit_mass = 1000*unit_length**3
    glysystem = UnitSystem(unit_length = unit_length, unit_mass=unit_mass,unit_time = 1e-6)
    #glysystem = UnitSystem(unit_length = unit_length, unit_mass=unit_mass,unit_energy="thermal")
    #glysystem.density = 1000    
    # dynamic viscosity water = 7e-4 Ns/m2
    print("REDUCED UNITS:")
    print("lengt fluid box = 1e-4m = ", 1e-4/glysystem.unit_length)
    print("dynamic viscosity = ", 1e-3/glysystem.unit_dynamic_viscosity)
    print("density 1000kg/m3 = ", 1e3/glysystem.unit_density)
    print("Young modulus solids 1Pa = ", 1/glysystem.unit_stress)
    print("Time step = 0.5*1e-4 = ", 0.5*1e-4/glysystem.unit_time)
    
    
    print("============ Magnitude of things =============")
    shear_rate = 50 #s-1
    Lz = 20.784609690826528 *glysystem.unit_length
    Lx = Lz
    print("Shear rate = 50s-1 =", shear_rate*glysystem.unit_time)
    
    print("Lx = Lz = ", Lx)
    
    Vxtop = shear_rate*Lz
    print("Vx in top = ", Vxtop)
    
    time_length = Lx/Vxtop
    print("time to travel one length = ", time_length)
    
    time_step = 50*glysystem.unit_time
    print("time_step = ", time_step)
    
    max_distance_time_step = time_step*Vxtop
    print("max_distance_time_step = ", max_distance_time_step, "in ru = ", max_distance_time_step/glysystem.unit_length)
 
    amax = Vxtop/time_step
    print("""
          Fluid acceleration. Assuming that Vx at the top is achieved 
          in one time-step, we have that a_max = Vx/time_step
          amax = 
          """, amax)
    
    print("amax in ru = ", amax /glysystem.unit_acceleration)
    
    print("""
          Since the density is 1 in the new reference units, we have that
          the neccesary volumetric force to achieve this acceleration
          is amax*rho =
          """, amax)
    
    vol_sphere = 4/3*np.pi*(glysystem.unit_length/2)**3
    print("volumen_particle = ", vol_sphere)
    print("amax*vol_sphere = ", amax*vol_sphere)
    
    print("force_on_particle (ru) = ", amax*vol_sphere/glysystem.unit_force)

    Re = 1.5
    Ri = 2/3*Re
    vol_object_3 = 4/3*np.pi*Re**3 - 4/3*np.pi*Ri**3
    Re = 0.5
    Ri = 2/3*Re
    vol_object_1 = 4/3*np.pi*Re**3 - 4/3*np.pi*Ri**3
    
    Re = 1
    Ri = 2/3*Re
    vol_object_2 = 4/3*np.pi*Re**3 - 4/3*np.pi*Ri**3
    
    f_o1 = 100/vol_object_3*vol_object_1
    f_o2 = 100/vol_object_3*vol_object_2
    
    
